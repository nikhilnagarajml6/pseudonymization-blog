import moviepy.editor as mp
import tqdm
import os
import cv2
import numpy as np
from tools.crop_face import *


def anonymize_video(video_path,save_path,cleanir,single=True,start_frame=0,end_frame=-1):
    """Anonymizes a given video using CleanIR
    
    Arguments:
        video_path (string): Video to be anonymized
        save_path (string): Location to store anonymized video
        cleanir (Cleanir): A cleanir object
        single (boolean): One or Multiple faces to anonymize in a frame (default=True)
        start_frame (int): The frame at which anonymizing starts. (default=0)
        end_frame (int):The frame at which anonymizing ends(default=-1 indicating the final frame)
        
    Returns:
        --
    
    """
    
    # Read original video
    original_video, frames, fps= read_original_video(video_path,start_frame,end_frame)
    
    # Create anonymized frames
    anon_frames = anonymize_frames(frames,cleanir,single)
    
    # Create anonymized video
    def make_frame(t):
        frame_idx = int(round(t * fps))
        anonymized_frame = anon_frames[frame_idx]
        return anonymized_frame

    anonymized_video = mp.VideoClip(make_frame)
    total_frames = int(original_video.duration * original_video.fps)
    end_frame = total_frames if end_frame is -1 else end_frame
    anonymized_video.duration = (end_frame - start_frame) / fps
    anonymized_video.fps = fps
    to_concatenate = []
    if start_frame != 0:
        to_concatenate.append(original_video.subclip(0, start_frame/fps))
    to_concatenate.append(anonymized_video)
    
    if end_frame != total_frames:
        to_concatenate.append(original_video.subclip(end_frame/fps, total_frames/fps))
    anonymized_video = mp.concatenate(to_concatenate)

    anonymized_video.audio = original_video.audio
    print("Anonymized video stats.")
    total_frames = int(anonymized_video.duration * anonymized_video.fps)
    print("Duration: {}. Total frames: {}, FPS: {}".format(anonymized_video.duration,total_frames,fps))
    print("Anonymizing from: {}({}), to: {}({})".format(start_frame,start_frame/fps,end_frame,end_frame/fps))

    anonymized_video.write_videofile(save_path, fps=original_video.fps,
                                     audio_codec='aac')
    

    
def read_original_video(video_path,start_frame=0,end_frame=-1):
    """Reads a given video and returns the frames to be anonymized
    
    Arguments:
        video_path (string): Video to be anonymized
        start_frame (int): The frame at which anonymizing starts. (default=0)
        end_frame (int):The frame at which anonymizing ends(default=-1)
        
    Returns:
        original_video (mp.VideoFile): The original video
        frames (list of numpy arrays): A list of frames to be anonymized
        fps: fps of original video
    
    """
    
    original_video = mp.VideoFileClip(video_path)
    fps = original_video.fps
    total_frames = int(original_video.duration * original_video.fps)
    start_frame = 0 if start_frame is 0 else start_frame
    end_frame = total_frames if end_frame is -1 else end_frame
    assert start_frame <= end_frame
    assert end_frame <= total_frames
    subclip = original_video.subclip(start_frame/fps, end_frame/fps)
    print("="*80)
    print("Anonymizing video.")
    print("Duration: {}. Total frames: {}, FPS: {}".format(original_video.duration,total_frames,fps))
    print("Anonymizing from: {}({}), to: {}({})".format(start_frame,start_frame/fps,end_frame,end_frame/fps))

    frames = list(tqdm.tqdm(subclip.iter_frames(), desc="Reading frames",
                            total=end_frame - start_frame))
    
    return original_video, frames, fps


def anonymize_frames(frames, cleanir, single=True, dsize=(64,64), deid_angle=180):
    """Anonymizes given frames and returns them in the same order
    
    Arguments:
        frames (list(np.array)): Frames to be anonymized
        cleanir (Cleanir): Anonymizer
        single (bool): Detects one face (default=True)
        dsize (tuple): (w,h) image size (default=64x64)
        deid_angle (float): Angle by which to transform the identity vector (default=180)
    
    Return:
        anon_frames (list(np.array)): anonymized frames
    """
    
    anon_frames = []
    
    if single:
        for frame in tqdm.tqdm(frames):

            face_crop, dim = crop_face_from_image_with_dim(frame, dsize, single)
            if face_crop and dim:
                t,r,b,l = dim[0][0], dim[0][1], dim[0][2], dim[0][3]
                anon_face_crop = cleanir.get_deid_single_axis_func(face_crop[0])(deid_angle)
                anon_face_crop = cv2.resize(anon_face_crop,(r-l,b-t))

                anony_frame = frame.copy()
                anony_frame[t:b,l:r] = anon_face_crop

                anon_frames.append(anony_frame)
            else:
                anon_frames.append(frame)
    else:
        for frame in tqdm.tqdm(frames):
            face_crops, dims = crop_face_from_image_with_dim(frame, dsize, single=False)
            if face_crops and dims:
                anony_frame = frame.copy()
                for face_crop,dim in zip(face_crops,dims):
                    t,r,b,l = dim[0], dim[1], dim[2], dim[3]
                    anon_face_crop = cleanir.get_deid_single_axis_func(face_crop)(deid_angle)
                    anon_face_crop = cv2.resize(anon_face_crop,(r-l,b-t))
                    anony_frame[t:b,l:r] = anon_face_crop

                anon_frames.append(anony_frame)
            else:
                anon_frames.append(frame)
                    
    
    return anon_frames
        
        
    
    
    