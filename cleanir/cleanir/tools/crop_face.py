import face_recognition
import cv2


def crop_face_from_image(image, dsize):
    """Detect and crop the largest face in the image

    Arguments:
        image {np.array} -- input image
        dsize {tuple} -- cropped face's size (w, h)

    Return:
        cropped face image
    """

    faces = face_recognition.face_locations(image, model="cnn")

    if len(faces) > 0:
        t, r, b, l = max(faces,
                         key=lambda face: (face[2] - face[0]) * (face[1] - face[3]))
        return cv2.resize(image[t:b, l:r], dsize)
    else:
        return cv2.resize(image, dsize)
    
def crop_face_from_image_dp(image, dsize):
    """Detect and crop the largest face in the image

    Arguments:
        image {np.array} -- input image
        dsize {tuple} -- cropped face's size (w, h)

    Return:
        cropped face image
    """

    faces = face_recognition.face_locations(image, model="cnn")

    if len(faces) > 0:
        t, r, b, l = max(faces,
                         key=lambda face: (face[2] - face[0]) * (face[1] - face[3]))
        return cv2.resize(image[t:b, l:r], dsize)
    else:
        None


def crop_face_from_file(filepath, dsize):
    """Detect and crop the largest face in the image

    Arguments:
        filepath {str} -- image file path
        dsize {tuple} -- cropped face's size (w, h)

    Return:
        cropped face image
    """

    image = face_recognition.load_image_file(filepath)
    return crop_face_from_image(image, dsize)

def crop_face_from_file_dp(filepath, dsize):
    """Detect and crop the largest face in the image

    Arguments:
        filepath {str} -- image file path
        dsize {tuple} -- cropped face's size (w, h)

    Return:
        cropped face image
    """

    image = face_recognition.load_image_file(filepath)
    return crop_face_from_image_dp(image, dsize)

def crop_face_from_image_with_dim(image, dsize, single=True):
    """Crops face from an image and returns the location of the face
    
    Arguments:
        image (np.array): A numpy array denoting the image
        dsize (tuple): cropped face's size (w,h)
        single (bool): Indicator which returns a single face or multiple faces from an image (default = True)
        
    Return:
        face_image (list(np.array)): cropped face image of dsize
        dsize (list(tuples)): found face locations
    """
    
    faces = face_recognition.face_locations(image, model="cnn")

    if len(faces) > 0 and single:
        t, r, b, l = max(faces,
                         key=lambda face: (face[2] - face[0]) * (face[1] - face[3]))
        return [cv2.resize(image[t:b, l:r], dsize)], [(t,r,b,l)]
    elif len(faces) > 0 and not single:
        return [cv2.resize(image[face[0]:face[2], face[3]:face[1]], dsize) for face in faces], faces   
    else:
        return [], []
