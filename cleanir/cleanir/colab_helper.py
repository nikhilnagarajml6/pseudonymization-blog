from cleanir import Cleanir
from tools.crop_face import *
from tools.get_model import *
from anonymize_video import anonymize_video
import cv2
import face_recognition

# Face image size
dsize = (64, 64)
# Angle by which to transform the face identity vector (Refer to the paper for more details)
deid_angle = 180


def anonymize_image(image_path):
    """
    Anonymizes given face and replaces the original with the anonymized face. 
    """

    #Load image and crop face
    image = face_recognition.load_image_file(image_path)
    face_crop, dim = crop_face_from_image_with_dim(image, dsize)

    if face_crop and dim:

        #Download Cleanir model
        MODEL_PATH = './model'

        # Download Cleanir model
        get_cleanir_model(MODEL_PATH)

        #Load cleanir model
        cleanir = Cleanir()
        cleanir.load_models(MODEL_PATH)

        #Anonymize face
        t,r,b,l = dim[0][0], dim[0][1], dim[0][2], dim[0][3]
        anon_face_crop = cleanir.get_deid_single_axis_func(face_crop[0])(deid_angle)
        anon_face_crop = cv2.resize(anon_face_crop,(r-l,b-t))

        #Replace original face with anonymized version
        anony_image = image.copy()
        anony_image[t:b,l:r] = anon_face_crop
        
        anony_image_write = cv2.cvtColor(anony_image, cv2.COLOR_RGB2BGR)
        cv2.imwrite(image_path[:-4] + '_anon' + '.jpg',anony_image_write)
    else:
        print("No face found!")

def colab_anonymize_video(input_path, output_path):
    """
    Anonymizes video at the input path and stores anonymized video at output path. 
    """

    #Download Cleanir model
    MODEL_PATH = './model'

    # Download Cleanir model
    get_cleanir_model(MODEL_PATH)

    #Load cleanir model
    cleanir = Cleanir()
    cleanir.load_models(MODEL_PATH)

    #Anonymize and store video
    anonymize_video(input_path,output_path,cleanir)

def anonymize_image_list(image_path_list):
  """
  Anonymizes a list of images whose paths are given in the image path list
  """

  for image_path in image_path_list:
    anonymize_image(image_path)

